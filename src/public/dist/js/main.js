$(document).ready(function () {
  const firebaseConfig = {
    apiKey: "AIzaSyCoEptpFZtjBNdfRWAEO6SoUqtv430R42g",
    authDomain: "messenger-sms-c9c98.firebaseapp.com",
    databaseURL: "https://messenger-sms-c9c98.firebaseio.com",
    projectId: "messenger-sms-c9c98",
    storageBucket: "",
    messagingSenderId: "839483218296",
    appId: "1:839483218296:web:091ee8b1a106032fcd108d"
  };

  firebase.initializeApp(firebaseConfig);

  window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  $('#login-phone-number').submit(function (event) {
    event.preventDefault();

    let phoneNumber = $('input[name="phoneNumber"]').val();
    if(phoneNumber[0] === '0' ) {
      phoneNumber = phoneNumber.replace("0",'+84');
    }
    const appVerifier = window.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        window.confirmationResult = confirmationResult;
        const firstName = $('input[name="firstName"]').val();
        const lastName = $('input[name="lastName"]').val();
        $('#login-phone-step1').remove();
        $('#login-phone-step2').css('display', 'block');

        $('#phone-number-verify').submit(function (event) {
          event.preventDefault();
          const code = $('input[name="code"]').val();
          confirmationResult.confirm(code).then(function (result) {
            var user = result.user;
            console.log(user);
            firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
              const body = {
                firstName,
                lastName,
                phoneNumber,
                idToken
              }
              $.ajax({
                type:'POST',
                url:'/login-sms',
                data: body,
                success : function(data) {
                    console.log(data);
                    if(data) {              
                        window.location.href = "/";
                    }
                    else {
                        alert('No!');
                    }
                }
              });
            });
            
            // $.post('/login-phone-number', body);
          }).catch(function (error) {
            console.log('error2', error);
          });
        });
      }).catch(function (error) {
        console.log('error', error);
      });
  })
  $('#register-email').submit(function (event) {
    event.preventDefault();
    const email = $('input[name="email"]').val();
    const password = $('input[name="password"]').val();

    // const password = getASecureRandomPassword(password);
    
    const firstName = $('input[name="firstName"]').val();
    const lastName = $('input[name="lastName"]').val();
    const userInfo = {
      firstName,
      lastName,
      email,
      password,
    }
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(function() {
        const user = firebase.auth().currentUser;
        user.sendEmailVerification()
        .then(function() {
          $.ajax({
            type:'POST',
            url:'/register-email',
            data: userInfo,
            success : function() {
                alert('Ye son , verify your email !!!');    
                window.location.assign("/login");  
            }
        }); 
        
    })
    .catch(err => {
        console.log(err);
    });
  })
  $('#register-sms').submit(function (event) {
    event.preventDefault();

    let phoneNumber = $('input[name="phoneNumber"]').val();

    if(phoneNumber[0] === '0' ) {
      phoneNumber = phoneNumber.replace('0','+84');
    }
    const appVerifier = window.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        window.confirmationResult = confirmationResult;
        const firstName = $('input[name="firstName"]').val();
        const lastName = $('input[name="lastName"]').val();
        $('#register-sms-step1').remove();
        $('#register-sms-step2').css('display', 'block');

        $('#register-sms-verify').submit(function (event) {
          event.preventDefault();
          const code = $('input[name="code"]').val();
          confirmationResult.confirm(code).then(function (result) {
            let user = result.user;
            data = {
              firstName,
              lastName,
              phoneNumber,
              // forceRefresh: user.refreshToken
            }
            
            $.ajax({
              type:'POST',
              url:'/register-sms',
              data,
              success : function() {
                alert('Ye boizzz');
                    
                window.location.assign("/login-sms");  
              }
            });
           

          }).catch(function (error) {
            console.log('error2', error);
            });
        });
      })
    })
  })
});
