
exports.up = (knex) => knex.schema
  .createTable('users', (table) => {
    table.increments('id').primary();
    table.string('firstName', 255).notNullable();
    table.string('lastName', 255).notNullable();
    table.string('avatar', 255).notNullable();
    table.string('email', 255).notNullable();
    table.string('phoneNumber', 255).notNullable();
    table.string('password', 255).notNullable();
    table.string('city', 255).notNullable();
    table.text('description');
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
  });
exports.down = (knex) => knex.schema
  .dropTableIfExists('users');
