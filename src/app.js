/* eslint-disable new-cap */
/* eslint-disable import/no-extraneous-dependencies */
import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import session from 'express-session';
import dotenv from 'dotenv';
import initRoutes from './config/routes';
// import pgSession from 'connect-pg-simple';
// const postgresSession = pgSession(session);

dotenv.config();

const app = express();
const pgSession = require('connect-pg-simple')(session);
const pgPool = require('pg').Pool;

const options = new pgPool({
  user: 'xmidou',
  password: 'midouss4',
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,
});

const sessionConfig = {
  store: new pgSession({
      pool: options,
      tableName: 'session',
  }),
  name: process.env.SE_NAME,
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
      aameSite: true,
      secure: false,
  },
};
  app.use(session(sessionConfig));


// view engine setup
app.set('views', path.join(__dirname, 'resources/views'));
app.set('view engine', 'pug');

// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

initRoutes(app);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
