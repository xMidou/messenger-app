import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/AuthService';

class AuthController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  registerByEmail(req, res) {
    return res.render('app/auth/register-email');
  }

  loginByEmail(req, res) {
    return res.render('app/login');
  }
}

export default AuthController;
