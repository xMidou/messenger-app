import express from 'express';
import * as admin from 'firebase-admin';
import Controller from '../Controllers/AuthController';
import knex from '../../../database/connection';

const router = express.Router();
const controller = new Controller();
router.route('/login')
  .get((req, res) => res.render('app/login'))
  .post(async (req, res) => {
    const { email, password, idToken } = req.body;
    const user = await knex('users').where({
      email: req.body.email,
      password: req.body.password,
    }).first();
    if (user) {
      return res.redirect('/');
    }
    admin.auth().getUserByEmail(email)
    .then(async (userRecord) => {
      await knex('users').insert({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        avatar: null,
        email,
        phoneNumber: null,
        password: req.body.password,
        city: null,
        description: null,
      });
      console.log('Successfully fetched user data:', userRecord.toJSON());
    })
    .catch((error) => {
    console.log('Error fetching user data:', error);
    });
    admin.auth().verifyIdToken(idToken)
    .then(async (decodedToken) => {
    const { uid } = decodedToken;
    if (user) {
      return res.render('/');
    }
    }).catch((error) => {
      window.alert('No boizzzzzz');
    });
  });
router.route('/login-sms')
  .get((req, res) => res.render('app/login-phone-number'))
  .post((req, res) => {
    admin.auth().verifyIdToken(idToken)
    .then(async (decodedToken) => {
      const { uid } = decodedToken;
      const user = await knex('users').where({
        phoneNumber: req.body.phoneNumber,
      }).first();
      if (user) {
        res.json(true);
        return res.render('/');
      }
      alert('No boizzzzzz');
    }).catch((error) => {
      res.json(false);
      alert('No boizzzzzz');
      console.log(error);
    });
  });
router.route('/register-email')
  .get((req, res) => res.render('app/register-email'))
  .post((req, res) => {
    res.redirect('/login');
  });
router.route('/register-sms')
  .get((req, res) => res.render('app/register-sms'))
  .post(async (req, res) => {
    await knex('users').insert({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      avatar: null,
      email: null,
      phoneNumber: req.body.phoneNumber,
      password: null,
      city: null,
      description: null,
  });


    res.redirect('/login-sms');
  });

router.get('/register', (req, res) => res.render('app/register'));

router.get('/reset-password', (req, res) => res.render('app/reset-password'));
router.get('/login', (req, res) => res.render('app/login'));

router.route('/login-phone-number')
  .get((req, res) => res.render('app/login-phone-number'));
  // .post((req, res) => {
  // });

router.get('/register', (req, res) => res.render('app/auth/register'));

router.get('/register-email', controller.callMethod('registerByEmail'));

router.get('/reset-password', (req, res) => res.render('app/reset-password'));

export default router;
