// require('dotenv').config();
module.exports = {
    development: {
        migrations: {
            directory: 'src/database/migrations',
            tableName: 'migrations',
        },
        client: 'pg',
        connection: {
          host: process.env.DB_HOST,
          user: 'xmidou',
          password: 'midouss4',
          port: process.env.DB_PORT,
          database: 'messenger-app',
        },
    },
    production: { client: 'pg', connection: process.env.DATABASE_URL },
};
